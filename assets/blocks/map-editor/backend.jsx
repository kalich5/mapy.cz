/* eslint-disable react/prop-types */

import React, { useRef, useEffect } from 'react';
import { registerBlockType } from '@wordpress/blocks';
import { __ } from '@wordpress/i18n';
import Select from 'react-select';
import { RichText } from '@wordpress/block-editor';
import { useSelect } from '@wordpress/data';
import { TextControl } from '@wordpress/components';
import styles from '../mapy-cz/backend.module.scss';
import renderMap, { parseMarkerFromApi } from '../../map';

const edit = (props) => {
  const {
    attributes,
    setAttributes,
    className,
  } = props;

  const {
    longitude,
    latitude,
    zoom,
    address,
    layer_type,
    layer_types,
    description,
    show_info_window,
    auto_center_zoom,
    map_type,
    markers: encoded_markers,
    width,
    height,
  } = attributes;

  const use_search = map_type === 'marker';
  const use_description = map_type === 'marker';
  const use_markers = map_type === 'map';
  const use_dimensions = map_type === 'map';

  const markers = encoded_markers ? JSON.parse(encoded_markers) : [];

  const allMarkers = use_markers ? useSelect(select => select('core').getEntityRecords('postType', 'wpify_mapycz_marker', { per_page: 100 })) : [];

  const markersOptions = allMarkers?.map((marker) => ({
    value: marker.id,
    label: marker.title.raw,
  }));

  const selectedMarkers = markersOptions?.filter((option) => markers?.includes(option.value));

  const mapMarkers = allMarkers?.filter(marker => markers?.includes(marker.id)).map(parseMarkerFromApi) || [];

  if (use_search) {
    mapMarkers.push({
      longitude,
      latitude,
    });
  }

  const rootRef = useRef();
  const suggestRef = useRef();
  const mapRefs = useRef();

  const handleMapChange = (e) => {
    const newCenter = e.target.getCenter();
    const newLatitude = newCenter.y;
    const newLongitude = newCenter.x;
    const newZoom = e.target.getZoom();

    if (latitude !== newLatitude || longitude !== newLongitude || newZoom !== zoom) {
      setAttributes({
        latitude: newLatitude,
        longitude: newLongitude,
        zoom: newZoom,
      });
    }
  };

  const handleLayerTypeChange = (type) => setAttributes({ layer_type: type.value });

  const handleAddressChange = (e) => setAttributes({ address: e.target.value });

  const handleDescriptionChange = (description) => setAttributes({ description });

  const processSuggest = (suggestData) => setAttributes({
    latitude: suggestData.data.latitude,
    longitude: suggestData.data.longitude,
    address: suggestData.data.phrase,
  });

  const handleMarkersChange = (maybeMarkers = []) => setAttributes({
    markers: JSON.stringify(
      Array.isArray(maybeMarkers) ? maybeMarkers.map(marker => marker.value) : []
    ),
  });

  const handleCenterZoomByMarkers = (event) => setAttributes({ auto_center_zoom: event.target.checked });

  const handleShowInfoWindow = (event) => setAttributes({ show_info_window: event.target.checked });

  const handleWidthChange = (width) => setAttributes({ width });

  const handleHeightChange = (height) => setAttributes({ height });

  // initialize the map
  useEffect(() => {
    const options = { zoom, latitude, longitude, layer_type, markers: mapMarkers, auto_center_zoom, show_info_window, handleMapChange };
    mapRefs.current = renderMap(rootRef.current, options, mapRefs.current);
  }, [use_search, zoom, latitude, longitude, layer_type, auto_center_zoom, show_info_window, mapMarkers, handleMapChange]);

  // initialize suggest
  useEffect(() => {
    if (use_search) {
      const suggest = new SMap.Suggest(suggestRef.current);
      suggest.addListener('suggest', processSuggest);
    }
  }, [use_search]);

  return (
    <div className={className}>
      {use_search && (
        <label
          style={{
            display: 'block',
            marginBottom: '10px',
          }}
        >
          <span style={{ marginRight: '10px' }}>
            {__('Search:', 'wpify-mapy-cz')}
          </span>
          <input
            type="text"
            value={address}
            ref={suggestRef}
            size={30}
            style={{ marginRight: '10px' }}
            onChange={handleAddressChange}
          />
          {latitude && longitude && (
            <div>
              <small>{' '}(lat: {latitude}, lon: {longitude})</small>
            </div>
          )}
        </label>
      )}
      <div className={styles.map} ref={rootRef} />
      {attributes.use_layer_type && (
        <label style={{ display: 'block', marginTop: '10px' }}>
          <div style={{ marginBottom: '10px' }}>
            <strong>{__('Display type:', 'wpify-mapy-cz')}</strong>
          </div>
          <Select
            value={{
              value: layer_type,
              label: layer_types[layer_type],
            }}
            options={Object.keys(layer_types).map(value => ({
              value,
              label: layer_types[value],
            }))}
            onChange={handleLayerTypeChange}
          />
        </label>
      )}
      {use_description && (
        <label style={{ display: 'block', marginTop: '10px' }}>
          <strong>{__('Description:', 'wpify-mapy-cz')}</strong>
          <RichText
            tagName="p"
            className={styles.description}
            onChange={handleDescriptionChange}
            value={description}
            placeholder={__('Description...', 'wpify-mapy-cz')}
            inlineToolbar
          />
        </label>
      )}
      {use_markers && markersOptions?.length > 0 && (
        <label style={{ display: 'block', marginTop: '10px' }}>
          <strong>{__('Markers:', 'wpify-mapy-cz')}</strong>
          <Select
            value={selectedMarkers}
            options={markersOptions}
            onChange={handleMarkersChange}
            isMulti
          />
        </label>
      )}
      {use_markers && markers.length > 0 && (
        <label style={{ display: 'block', marginTop: '10px' }}>
          <input
            type="checkbox"
            checked={auto_center_zoom}
            onChange={handleCenterZoomByMarkers}
          />
          {__('Set zoom and center by markers', 'wpify-mapy-cz')}
        </label>
      )}
      {use_markers && (
        <label style={{ display: 'block', marginTop: '10px' }}>
          <input
            type="checkbox"
            checked={show_info_window}
            onChange={handleShowInfoWindow}
          />
          {__('Show info window', 'wpify-mapy-cz')}
        </label>
      )}
      {use_dimensions && (
        <div>
          <TextControl
            label={__('Width:', 'wpify-mapy-cz')}
            value={width}
            onChange={handleWidthChange}
          />
          <TextControl
            label={__('Height:', 'wpify-mapy-cz')}
            value={height}
            onChange={handleHeightChange}
          />
        </div>
      )}
    </div>
  );
};

const save = () => null;

registerBlockType('wpify/map-editor', {
  title: __('WPify Map Editor', 'wpify-mapy-cz'),
  description: __('Editor for WPify Mapy.cz!', 'wpify-mapy-cz'),
  icon: 'grid-view',
  category: 'embed',
  supports: {
    inserter: false,
    align: false,
    alignWide: false,
    defaultStylePicker: false,
    anchor: false,
    customClassName: false,
    className: false,
    html: false,
    multiple: false,
    reusable: false,
  },
  edit,
  save,
});
