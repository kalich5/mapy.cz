/* eslint-disable react/prop-types */

import React, { useRef, useEffect } from 'react';
import { registerBlockType } from '@wordpress/blocks';
import { __ } from '@wordpress/i18n';
import { SelectControl } from '@wordpress/components';
import { useSelect } from '@wordpress/data';
import renderMap, { parseMarkerFromApi, parseMapFromApi } from '../../map';
import styles from './backend.module.scss';

const edit = (props) => {
  const {
    attributes,
    setAttributes,
    className,
  } = props;

  const renderRef = useRef();
  const mapRef = useRef();

  const maps = useSelect(select => select('core').getEntityRecords('postType', 'wpify_mapycz_map', { per_page: 100 }));

  const currentMap = maps?.find(map => map.id === parseInt(attributes.map_id, 10));

  const rawMarkers = useSelect(select => select('core').getEntityRecords('postType', 'wpify_mapycz_marker', {
    include: currentMap ? JSON.parse(currentMap.meta.markers) : [],
    per_page: currentMap ? 100 : 1,
  }));

  const markers = rawMarkers && rawMarkers.map(parseMarkerFromApi);

  useEffect(() => {
    if (currentMap) {
      const options = parseMapFromApi(currentMap, markers);
      renderRef.current = renderMap(mapRef.current, options, renderRef.current);
    }
  }, [currentMap, markers]);

  if (typeof maps === 'undefined' || !maps) {
    return (
      <h2>Loading maps list...</h2>
    )
  }

  const mapsOptions = [
    {
      label: __('Select map', 'wpify-mapy-cz'),
      value: ''
    }
  ];

  maps.map((item) => mapsOptions.push({value: item.id, label: item.title.rendered}));

  return (
    <div className={className}>
      <SelectControl
        label={__('WPify Mapy.cz:', 'wpify-mapy-cz')}
        options={mapsOptions}
        onChange={(map_id) => setAttributes({ map_id })}
        value={attributes.map_id}
      />
      <div ref={mapRef} style={{ height: '400px', display: currentMap ? 'block' : 'none' }} className={styles.map} />
    </div>
  );
};

const save = () => null;

registerBlockType('wpify/mapy-cz', {
  title: __('WPify Mapy.cz', 'wpify-mapy-cz'),
  icon: 'grid-view',
  category: 'embed',
  edit,
  save,
});

