const renderMap = (element, options, currentMap) => {
  const {
    layer_type = 'DEF_BASE',
    zoom = 1,
    auto_center_zoom = false,
    show_info_window = false,
    latitude = 0,
    longitude = 0,
    markers = [],
    handleMapChange = null,
  } = options;

  let newMap = {};

  if (currentMap && currentMap.map && currentMap.defaultLayer) {
    // rerender current map
    newMap = { ...currentMap };

    if (currentMap.latitude !== latitude || currentMap.longitude !== longitude) {
      newMap.map.setCenter(SMap.Coords.fromWGS84(longitude, latitude));
    }

    if (currentMap.zoom !== zoom) {
      newMap.map.setZoom(zoom);
    }

    if (currentMap.layer_type !== layer_type) {
      newMap.map.removeLayer(newMap.defaultLayer);
      newMap.defaultLayer = newMap.map.addDefaultLayer(SMap[layer_type]);
      newMap.defaultLayer.enable();
    }
  } else if (element) {
    // create a new map
    newMap.map = new SMap(element, SMap.Coords.fromWGS84(longitude, latitude), zoom, {
      animTime: 0,
      zoomTime: 0,
      rotationTime: 0,
    });
    newMap.defaultLayer = newMap.map.addDefaultLayer(SMap[layer_type]);
    newMap.defaultLayer.enable();
    newMap.markerLayer = new SMap.Layer.Marker();
    newMap.map.addLayer(newMap.markerLayer);
    newMap.markerLayer.enable();
    newMap.map.addDefaultControls();

    if (handleMapChange) {
      newMap.map.getSignals().addListener(window, 'map-redraw', handleMapChange);
    }
  }

  newMap.markerLayer.removeAll();

  const markersCoords = [];

  Array.isArray(markers) && markers.forEach((marker) => {
    const coords = SMap.Coords.fromWGS84(marker.longitude, marker.latitude);
    markersCoords.push(coords);

    const newMarker = new SMap.Marker(coords, null, {
      title: marker.title,
      anchor: { left: 10, bottom: 1 },
    });

    if (show_info_window) {
      const newCard = new SMap.Card();

      if (marker.title) {
        newCard.getHeader().innerHTML = `<h4 class="mapycz-card-header mapycz-card-header--${marker.id}">${marker.title}</h4>`;
      }

      if (marker.description) {
        newCard.getBody().innerHTML = `<div class="mapycz-card-body mapycz-card-body--${marker.id}">${marker.description}</div>`;
      }

      if (marker.address) {
        newCard.getFooter().innerHTML = `<div class="mapycz-card-footer mapycz-card-footer--${marker.id}">${marker.address}</div>`;
      }

      newMarker.decorate(SMap.Marker.Feature.Card, newCard);
    }

    newMap.markerLayer.addMarker(newMarker);
  });

  if (auto_center_zoom) {
    const coords = Array.isArray(markers) && markers.map(marker => SMap.Coords.fromWGS84(marker.longitude, marker.latitude));

    if (coords) {
      const [newCenter, newZoom] = newMap.map.computeCenterZoom(coords);

      if (newMap.longitude !== newCenter.x || newMap.latitude !== newCenter.y) {
        newMap.map.setCenter(newCenter);
      }

      if (newMap.zoom !== newZoom) {
        newMap.map.setZoom(newZoom);
      }
    }
  }

  return { ...newMap, ...options };
};

export const parseMarkerFromApi = (marker) => ({
  id: marker.id,
  longitude: marker.meta.longitude,
  latitude: marker.meta.latitude,
  title: marker.title.rendered,
  description: marker.meta.description,
  address: marker.meta.address,
});

export const parseMapFromApi = (map, markers = []) => ({
  layer_type: map.meta.layer_type,
  zoom: map.meta.zoom,
  auto_center_zoom: map.meta.auto_center_zoom,
  show_info_window: map.meta.show_info_window,
  latitude: map.meta.latitude,
  longitude: map.meta.longitude,
  markers,
});

export default renderMap;
