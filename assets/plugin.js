import renderMap from './map';

const renderAllMaps = () => {
  document.querySelectorAll('[data-mapycz]').forEach((element) => {
    const data = JSON.parse(element.dataset.mapycz);
    const options = {
      layer_type: data.default_layer,
      zoom: parseInt(data.zoom, 10),
      auto_center_zoom: Boolean(parseInt(data.center_auto, 10)),
      show_info_window: Boolean(parseInt(data.show_info_window, 10)),
      latitude: parseFloat(data.centerLat),
      longitude: parseFloat(data.centerLng),
      markers: data.markers?.map((marker) => ({
        title: marker.title,
        description: marker.description,
        address: marker.address,
        longitude: parseFloat(marker.longitude),
        latitude: parseFloat(marker.latitude),
      })),
    };

    renderMap(element, options);
  });
};

window.addEventListener('DOMContentLoaded', renderAllMaps);
