��    A      $  Y   ,      �     �     �  _   �  W        d     m  	   y     �     �     �     �     �  H   �     !     *     6     P  ,   X  
   �     �     �     �     �     �  
   �     �     �     �       	             ,  $   <     a     y     �     �  
   �     �  	   �     �  �   �  
   �	     �	     �	     �	     �	     �	     �	  y   �	     U
     \
  S   n
     �
     �
     �
               0     4     ;     W     v     �  �  �     ?     R  m   h  W   �     .     <     M     ]     i     x     �     �  f   �               ,     F  2   O     �     �     �     �     �  
   �     �     �     �  )   �  !   )  
   K     V     i  (        �     �     �  	   �     �  >   �  	   8     B  �   S     �               +     1     =     K  �   Z  	   �     �  T        `     e     m     r     z     �     �     �     �     �     �            !   -   "      <       7                 6      )         >          ,                (          3   *      ;          %                 .          5   4   &         =          @               $      8              :   '   9                   ?      #          A   	       +       /           
   1           0   2    Add New Map Add New Marker Add a new <a href="%s">map</a>, enter the options, choose markers to display and click Publish. Add as many <a href="%s">markers</a> as needed, select it's positions and publish them. All Maps All Markers Basic map Description of Map. Description of Marker. Description... Description: Display type: Easily embed free mapy.cz maps on your posts / pages / Custom Post Types Edit Map Edit Marker Editor for WPify Mapy.cz! Height: How to create map and insert it to your page Hybrid map Maps Mapy.cz Markers Markers: New Map New Marker No Maps found in Trash. No Maps found. No Markers found in Trash. No Markers found. Ortho map Parent Maps: Parent Markers: Please add id param to the shortcode Please provide valid ID Search Maps Search Markers Search: Select map Set zoom and center by markers Shortcode Show info window To insert the map into your page, you can either use <i>Mapy.cz</i> Gutenberg block, or you can use the shortcode provided in the maps <a href="%s">admin listing</a> Turist map View Map View Marker WPify WPify Map Editor WPify Mapy.cz WPify Mapy.cz: Welcome! Plugin WPify Mapy.cz by <a href="%s" target="_blank">wpify.io</a> lets you add mapy.cz maps to your site easily! Width: Winter turist map You can start adding <a href="%s">maps</a> and <a href="%s">markers</a> right away. add new on admin barMap add new on admin barMarker admin menuMaps admin menuMarkers https://www.wpify.io/ map marker post type general nameMaps post type general nameMarkers post type singular nameMap post type singular nameMarker Project-Id-Version: Mapy.cz 1.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/wpify-mapy-cz
PO-Revision-Date: 2020-09-18 21:00+0200
Last-Translator: 
Language-Team: 
Language: cs_CZ
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.1
X-Domain: mapy-cz
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n>=2 && n<=4 ? 1 : 2);
 Přidat novou mapu Přidat novou značku Přidejte novou <a href="%s">mapu</a>, přizpůsobte ji, vyberte značky k zobrazení a publikujte svou mapu. Přidejte <a href="%s">značek</a> kolik chcete, vyberte jejich pozici a publikujte je. Všechny mapy Všechny značky Základní mapa Popis mapy. Popis značky. Popis značky… Popis značky: Typ zobrazení: Přidávejte snadno a zdarma Mapy.cz do vašich článků, stránek nebo jiných vlastních post typů Upravit mapu Upravit značku Editor pro WPify Mapy.cz! Výška: Jak vytvořit mapy a vložit je na vaší stránku Hybridní mapa Mapy Mapy.cz Značky Značky: Nová mapa Nová značka V koši není žádná mapa. Žádná mapa nenalezena. Žádná značka nebyla nalezena v koši. Žádná značka nebyla nalezena. Ortho mapa Nadřízená mapa: Nadřízená značka: Prosím zadejte id parametr do shortcode Prosím zadejte platné ID Vyhledat mapu Vyhledat značky Vyhledat: Vyberte mapu Automaticky nastavit zoom a střed podle zobrazených markerů Shortcode Zobrazit vizitku Pro přidání mapy na stránku můžete využít jak <i>Mapy.cz</i> gutenberg block, nebo můžete použít shortcode, který naleznete v <a href="%s">seznamu map</a> Turistická mapa Zobrazit mapu Zobrazit značku WPify Editor mapy WPify Mapy.cz WPify Mapy.cz: Vítejte! Plugin WPify Mapy.cz od <a href="%s" target="_blank">wpify.io</a> vám umožní snadno přidávat mapy od Mapy.cz na vaše stránky! Šířka: Zimní turistická mapa Můžete rovnou začít přidávat <a href="%s">mapy</a> a <a href="%s">značky</a>. Mapa Značka Mapy Značky https://www.wpify.io/ mapa značka Mapy Značky Mapa Značky 