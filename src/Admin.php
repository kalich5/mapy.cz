<?php

namespace WPifyMapyCz;

use WPifyMapyCz\Cpt\MapPostType;
use WPifyMapyCz\Cpt\MarkerPostType;
use Wpify\Core\AbstractController;

class Admin extends AbstractController
{
  /**
   * Register hooks
   * @return bool|void
   */
  public function setup()
  {
    add_action('admin_menu', [$this, 'add_menu_pages']);
    add_filter('manage_' . MapPostType::NAME . '_posts_columns', [$this, 'add_admin_columns'], 10);
    add_action('manage_' . MapPostType::NAME . '_posts_custom_column', [$this, 'admin_columns_content'], 10, 2);
  }

  /**
   * Add admin menu pages
   */
  public function add_menu_pages()
  {
    add_menu_page(
      __('Mapy.cz', 'wpify-mapy-cz'),
      __('Mapy.cz', 'wpify-mapy-cz'),
      apply_filters('mapy_cz_menu_capability', 'manage_options'),
      'wpify-mapy-cz',
      [$this, 'render_main_menu_page'],
      $this->plugin->get_asset_url('assets/images/mapy-cz-small.jpg'),
      6
    );
    add_submenu_page(
      'wpify-mapy-cz',
      __('Maps', 'wpify-mapy-cz'),
      __('Maps', 'wpify-mapy-cz'),
      apply_filters('mapy_cz_menu_capability', 'manage_options'),
      $this->get_admin_url_maps(),
      null
    );
    add_submenu_page(
      'wpify-mapy-cz',
      __('Markers', 'wpify-mapy-cz'),
      __('Markers', 'wpify-mapy-cz'),
      apply_filters('mapy_cz_menu_capability', 'manage_options'),
      $this->get_admin_url_markers(),
      null
    );
  }

  /**
   * Get admin URL for maps
   * @return string
   */
  public function get_admin_url_maps()
  {
    return sprintf('edit.php?post_type=%s', MapPostType::NAME);
  }

  /**
   * Get admin URL for markers
   * @return string
   */
  public function get_admin_url_markers()
  {
    return sprintf('edit.php?post_type=%s', MarkerPostType::NAME);
  }

  /**
   * Render main admin menu page
   */
  public function render_main_menu_page()
  { ?>
    <div class="wrap">
      <h2><?php _e('WPify Mapy.cz', 'wpify-mapy-cz') ?></h2>
      <p><?php printf(
          __(
            'Welcome! Plugin WPify Mapy.cz by <a href="%s" target="_blank">wpify.io</a> lets you add mapy.cz maps to your site easily!',
            'wpify-mapy-cz'
          ),
          'https://wpify.io'
        ) ?></p>
      <p><?php printf(
          __(
            'You can start adding <a href="%s">maps</a> and <a href="%s">markers</a> right away.',
            'wpify-mapy-cz'
          ),
          $this->get_admin_url_maps(),
          $this->get_admin_url_markers()
        ) ?></p>
      <h3><?php _e('How to create map and insert it to your page', 'wpify-mapy-cz'); ?></h3>
      <ol>
        <li><?php printf(
            __('Add as many <a href="%s">markers</a> as needed, select it\'s positions and publish them.', 'wpify-mapy-cz'),
            $this->get_admin_url_markers()
          ) ?>
        </li>
        <li><?php printf(
            __(
              'Add a new <a href="%s">map</a>, enter the options, choose markers to display and click Publish.',
              'wpify-mapy-cz'
            ),
            $this->get_admin_url_maps()
          ) ?>
        </li>
        <li><?php printf(
            __(
              'To insert the map into your page, you can either use <i>Mapy.cz</i> Gutenberg block, or you can use the shortcode provided in the maps <a href="%s">admin listing</a>',
              'wpify-mapy-cz'
            ),
            $this->get_admin_url_maps()
          ) ?>
        </li>
      </ol>
    </div>
  <?php }

  public function add_admin_columns($columns)
  {
    $columns['shortcode'] = __('Shortcode', 'wpify-mapy-cz');

    return $columns;
  }

  public function admin_columns_content($column_name, $post_id)
  {
    switch ($column_name) {
      case 'shortcode':
        printf('[wpify-mapy-cz id="%s"]', $post_id);
        break;
      default:
    }
  }
}
