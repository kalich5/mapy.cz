<?php

namespace WPifyMapyCz;

use WPifyMapyCz\Blocks\MapyCzBlock;
use Wpify\Core\AbstractAssets;

/**
 * @property Plugin $plugin
 */
class Assets extends AbstractAssets
{
  /**
   * Enqueue assets
   * @return array
   */
  public function assets(): array
  {
    $this->enqueue_main_scripts();
    if (!$this->do_enqueue()) {
      return [];
    }

    $assets = array_merge(
      $this->get_manifest_asset('plugin.js', 'mapy-cz', [], ['mapy-cz-loader']),
    );

    return $assets;
  }

  public function enqueue_main_scripts()
  {
    wp_enqueue_script('mapy-cz-loader', 'https://api.mapy.cz/loader.js', [], false, true);
    wp_add_inline_script('mapy-cz-loader', 'Loader.load(null, {suggest: true});');
  }

  /**
   * Check if the scripts should be enqueued
   * @return bool
   */
  public function do_enqueue()
  {
    global $post;
    if (!is_a($post, 'WP_Post')) {
      return false;
    }

    if (has_block($this->plugin->get_blocks_manager()->get_module(MapyCzBlock::class)->name(), $post)) {
      return true;
    };

    if (has_shortcode($post->post_content, $this->plugin->get_frontend()::SHORTCODE_TAG)) {
      return true;
    }

    return false;
  }
}
