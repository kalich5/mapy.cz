<?php

namespace WPifyMapyCz\Blocks;

use WPifyMapyCz\Plugin;
use Wpify\Core\AbstractBlock;

/** @property Plugin $plugin */
class MapEditorBlock extends AbstractBlock
{
  public function register(): void
  {
    wp_register_script('mapy-cz-loader', 'https://api.mapy.cz/loader.js', [], false, true);
    wp_add_inline_script('mapy-cz-loader', 'Loader.load(null, {suggest: true});');

    register_block_type($this->name(), [
      'attributes'      => $this->attributes(),
      'editor_script'   => $this->plugin->get_assets()->register_manifest_asset('map-editor-backend.js', 'wpify-map-editor-backend', null, ['mapy-cz-loader']),
      'editor_style'    => $this->plugin->get_assets()->register_manifest_asset('map-editor-backend.css'),
    ]);

    wp_set_script_translations('wpify-map-editor-backend', 'wpify-mapy-cz', $this->plugin->get_asset_path('languages'));
  }

  public function name(): string
  {
    return 'wpify/map-editor';
  }

  public function attributes(): array
  {
    return [
      'description'      => [
        'type'    => 'string',
        'source'  => 'meta',
        'meta'    => 'description',
        'default' => '',
      ],
      'longitude'        => [
        'type'    => 'number',
        'source'  => 'meta',
        'meta'    => 'longitude',
        'default' => 0,
      ],
      'latitude'         => [
        'type'    => 'number',
        'source'  => 'meta',
        'meta'    => 'latitude',
        'default' => 0,
      ],
      'address'          => [
        'type'    => 'string',
        'source'  => 'meta',
        'meta'    => 'address',
        'default' => '',
      ],
      'zoom'             => [
        'type'    => 'integer',
        'source'  => 'meta',
        'meta'    => 'zoom',
        'default' => 1,
      ],
      'layer_type'       => [
        'type'    => 'string',
        'source'  => 'meta',
        'meta'    => 'layer_type',
        'default' => 'DEF_BASE',
      ],
      'markers'          => [
        'type'    => 'string',
        'source'  => 'meta',
        'meta'    => 'markers',
        'default' => '[]',
      ],
      'auto_center_zoom' => [
        'type'    => 'boolean',
        'source'  => 'meta',
        'meta'    => 'auto_center_zoom',
        'default' => false,
      ],
      'show_info_window' => [
        'type'    => 'boolean',
        'source'  => 'meta',
        'meta'    => 'show_info_window',
        'default' => false,
      ],
      'map_type'         => [
        'type' => 'string',
      ],
      'layer_types'      => [
        'type'    => 'array',
        'default' => [
          'DEF_BASE'          => __('Basic map', 'wpify-mapy-cz'),
          'DEF_OPHOTO'        => __('Ortho map', 'wpify-mapy-cz'),
          'DEF_HYBRID'        => __('Hybrid map', 'wpify-mapy-cz'),
          'DEF_TURIST'        => __('Turist map', 'wpify-mapy-cz'),
          'DEF_TURIST_WINTER' => __('Winter turist map', 'wpify-mapy-cz'),
        ],
      ],
      'width'            => [
        'type'    => 'string',
        'source'  => 'meta',
        'meta'    => 'width',
        'default' => '100%',
      ],
      'height'           => [
        'type'    => 'string',
        'source'  => 'meta',
        'meta'    => 'height',
        'default' => '400px',
      ],
    ];
  }
}
