<?php

namespace WPifyMapyCz\Blocks;

use WPifyMapyCz\Plugin;
use Wpify\Core\AbstractBlock;

/** @property Plugin $plugin */
class MapyCzBlock extends AbstractBlock
{
  public function name(): string
  {
    return 'wpify/mapy-cz';
  }

  public function attributes(): array
  {
    return [
      'map_id' => [
        'type'    => 'string',
        'default' => null,
      ],
    ];
  }

  public function register(): void
  {
    register_block_type(
      $this->name(),
      [
        'attributes'      => $this->attributes(),
        'render_callback' => [$this, 'render'],
        'editor_script'   => $this->plugin->get_assets()->register_manifest_asset('block-mapy-cz-backend.js', 'wpify-mapy-cz-backend'),
      ]
    );

    wp_set_script_translations('wpify-mapy-cz-backend', 'wpify-mapy-cz', $this->plugin->get_asset_path('languages'));
  }

  public function render($block_attributes, $content): string
  {
    if (is_admin()) {
      return '';
    }

    $attributes = $this->parse_attributes($block_attributes);

    return $this->plugin->get_frontend()->get_map_markup($attributes['map_id']);
  }
}
