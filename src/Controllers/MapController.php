<?php

namespace WPifyMapyCz\Controllers;

use WPifyMapyCz\Models\MapModel;
use WPifyMapyCz\Plugin;
use WPifyMapyCz\Controller;
use WPifyMapyCz\Repositories\MapRepository;

/**
 * @property Plugin $plugin
 */
class MapController extends Controller
{
  public function get_map($id)
  {
    /** @var MapModel $map */
    $map     = $this->plugin->get_repository(MapRepository::class)->get($id);
    $markers = [];

    foreach ($map->get_markers() as $marker) {
      $markers[] = $marker->to_array();
    }

    $data = [
      'id'                 => $map->get_id(),
      'title'              => $map->get_title(),
      'centerLat'          => $map->get_center_lat(),
      'centerLng'          => $map->get_center_lng(),
      'zoom'               => $map->get_zoom(),
      'width'              => $map->get_width(),
      'height'             => $map->get_height(),
      'markers'            => $markers,
      'markers_categories' => $map->get_markers_categories(),
      'hash'               => $map->get_hash(),
      'show_info_window'   => $map->get_show_info_window(),
      'center_auto'        => $map->get_center_auto(),
      'default_layer'      => $map->get_default_layer(),
    ];

    return apply_filters('wpify_mapy_cz_map_data', $data, $map);
  }
}
