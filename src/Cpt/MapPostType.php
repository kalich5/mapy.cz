<?php

namespace WPifyMapyCz\Cpt;

use WPifyMapyCz\Factories\MetaFieldsFactory;
use WPifyMapyCz\Models\MapModel;
use WPifyMapyCz\Plugin;
use Exception;
use Wpify\Core\AbstractPostType;

/**
 * Class MapPostType
 * @package WPifyMapyCz\Cpt
 * @property Plugin $plugin
 */
class MapPostType extends AbstractPostType
{
  public const NAME = 'wpify_mapycz_map';

  public function post_type_args(): array
  {
    return [
      'labels'             => [
        'name'               => _x('Maps', 'post type general name', 'wpify-mapy-cz'),
        'singular_name'      => _x('Map', 'post type singular name', 'wpify-mapy-cz'),
        'menu_name'          => _x('Maps', 'admin menu', 'wpify-mapy-cz'),
        'name_admin_bar'     => _x('Map', 'add new on admin bar', 'wpify-mapy-cz'),
        'add_new'            => __('Add New', 'add new', 'wpify-mapy-cz'),
        'add_new_item'       => __('Add New Map', 'wpify-mapy-cz'),
        'new_item'           => __('New Map', 'wpify-mapy-cz'),
        'edit_item'          => __('Edit Map', 'wpify-mapy-cz'),
        'view_item'          => __('View Map', 'wpify-mapy-cz'),
        'all_items'          => __('All Maps', 'wpify-mapy-cz'),
        'search_items'       => __('Search Maps', 'wpify-mapy-cz'),
        'parent_item_colon'  => __('Parent Maps:', 'wpify-mapy-cz'),
        'not_found'          => __('No Maps found.', 'wpify-mapy-cz'),
        'not_found_in_trash' => __('No Maps found in Trash.', 'wpify-mapy-cz'),
      ],
      'description'        => __('Description of Map.', 'wpify-mapy-cz'),
      'public'             => false,
      'publicly_queryable' => true,
      'show_ui'            => true,
      'show_in_menu'       => false,
      'show_in_rest'       => true,
      'query_var'          => true,
      'rewrite'            => ['slug' => __('map', 'wpify-mapy-cz')],
      'capability_type'    => 'post',
      'has_archive'        => false,
      'hierarchical'       => false,
      'menu_position'      => null,
      'supports'           => ['title', 'editor', 'custom-fields'],
      'template'           => [
        ['wpify/map-editor', [
          'map_type'        => 'map',
        ]],
      ],
      'template_lock'      => 'all',
    ];
  }

  public function post_type_name(): string
  {
    return $this::NAME;
  }

  public function model(): string
  {
    return MapModel::class;
  }

  public function custom_fields()
  {
    return [
      [
        'meta_key' => 'longitude',
        'type'     => 'number',
        'default'  => 0,
      ],
      [
        'meta_key' => 'latitude',
        'type'     => 'number',
        'default'  => 0,
      ],
      [
        'meta_key' => 'zoom',
        'type'     => 'integer',
        'default'  => 1,
      ],
      [
        'meta_key' => 'layer_type',
        'type'     => 'string',
        'default'  => 'DEF_BASE',
      ],
      [
        'meta_key' => 'width',
        'type'     => 'string',
        'default'  => '100%',
      ],
      [
        'meta_key' => 'height',
        'type'     => 'string',
        'default'  => '500px',
      ],
      [
        'meta_key' => 'markers',
        'type'     => 'string',
        'default'  => '[]',
      ],
      [
        'meta_key' => 'auto_center_zoom',
        'type'     => 'boolean',
        'default'  => false,
      ],
      [
        'id'      => 'show_info_window',
        'type'    => 'boolean',
        'default' => true,
      ],
    ];
  }

  public function custom_fields_factory(): string
  {
    return MetaFieldsFactory::class;
  }
}
