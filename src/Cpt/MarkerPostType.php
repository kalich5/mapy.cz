<?php

namespace WPifyMapyCz\Cpt;

use WPifyMapyCz\Factories\MetaFieldsFactory;
use WPifyMapyCz\Models\MarkerModel;
use Wpify\Core\AbstractPostType;

class MarkerPostType extends AbstractPostType
{
  public const NAME = 'wpify_mapycz_marker';

  public function post_type_args(): array
  {
    return [
      'labels'             => [
        'name'               => _x('Markers', 'post type general name', 'wpify-mapy-cz'),
        'singular_name'      => _x('Marker', 'post type singular name', 'wpify-mapy-cz'),
        'menu_name'          => _x('Markers', 'admin menu', 'wpify-mapy-cz'),
        'name_admin_bar'     => _x('Marker', 'add new on admin bar', 'wpify-mapy-cz'),
        'add_new'            => __('Add New', 'add new', 'wpify-mapy-cz'),
        'add_new_item'       => __('Add New Marker', 'wpify-mapy-cz'),
        'new_item'           => __('New Marker', 'wpify-mapy-cz'),
        'edit_item'          => __('Edit Marker', 'wpify-mapy-cz'),
        'view_item'          => __('View Marker', 'wpify-mapy-cz'),
        'all_items'          => __('All Markers', 'wpify-mapy-cz'),
        'search_items'       => __('Search Markers', 'wpify-mapy-cz'),
        'parent_item_colon'  => __('Parent Markers:', 'wpify-mapy-cz'),
        'not_found'          => __('No Markers found.', 'wpify-mapy-cz'),
        'not_found_in_trash' => __('No Markers found in Trash.', 'wpify-mapy-cz'),
      ],
      'description'        => __('Description of Marker.', 'wpify-mapy-cz'),
      'public'             => false,
      'publicly_queryable' => true,
      'show_ui'            => true,
      'show_in_menu'       => false,
      'show_in_rest'       => true,
      'query_var'          => true,
      'rewrite'            => ['slug' => __('marker', 'wpify-mapy-cz')],
      'capability_type'    => 'post',
      'has_archive'        => false,
      'hierarchical'       => false,
      'menu_position'      => null,
      'supports'           => ['title', 'editor', 'custom-fields'],
      'template'           => [
        ['wpify/map-editor', [
          'map_type'        => 'marker',
        ]],
      ],
      'template_lock'      => 'all',
    ];
  }

  public function post_type_name(): string
  {
    return $this::NAME;
  }

  public function model(): string
  {
    return MarkerModel::class;
  }

  public function custom_fields()
  {
    return [
      [
        'meta_key' => 'description',
        'type'     => 'string',
        'default'  => '',
      ],
      [
        'meta_key' => 'longitude',
        'type'     => 'number',
        'default'  => 0,
      ],
      [
        'meta_key' => 'latitude',
        'type'     => 'number',
        'default'  => 0,
      ],
      [
        'meta_key' => 'zoom',
        'type'     => 'integer',
        'default'  => 1,
      ],
      [
        'meta_key' => 'layer_type',
        'type'     => 'string',
        'default'  => 'DEF_BASE',
      ],
      [
        'meta_key' => 'address',
        'type'     => 'string',
        'default'  => '',
      ],
    ];
  }

  public function custom_fields_factory(): string
  {
    return MetaFieldsFactory::class;
  }

}
