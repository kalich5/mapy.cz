<?php

namespace WPifyMapyCz\Factories;

use InvalidArgumentException;
use WPifyMapyCz\Plugin;
use Wpify\Core\AbstractCustomFieldsFactory;

/** @property Plugin $plugin */
class MetaFieldsFactory extends AbstractCustomFieldsFactory
{
  /** @var array $custom_fields Array of the custom fields */
  private $custom_fields = [];

  /**
   * Register hooks
   * @return bool|void
   */
  public function setup()
  {
    add_action('init', [$this, 'register_fields']);
  }

  public function get_default_args()
  {
    return [
      'object_type'       => 'post',
      'meta_key'          => null,
      'object_subtype'    => $this->get_entity_name(),
      'type'              => 'string',
      'description'       => '',
      'single'            => true,
      'default'           => '',
      'sanitize_callback' => null,
      'auth_callback'     => null,
      'show_in_rest'      => true,
    ];
  }

  public function set_custom_fields(array $custom_fields): void
  {
    $this->custom_fields = [];
    $allowed_types = ['string', 'boolean', 'integer', 'number', 'array', 'object'];

    foreach($custom_fields as $field) {
      $args = wp_parse_args($field, $this->get_default_args());

      $aliases = [
        'desc' => 'description',
        'id' => 'meta_key',
      ];

      foreach ($aliases as $alias => $final) {
        if (isset($args[$alias])) {
          $args[$final] = $args[$alias];
          unset($args[$alias]);
        }
      }

      if (in_array($args['type'], $allowed_types)) {
        $this->custom_fields[] = $args;
      } else {
        throw new InvalidArgumentException(sprintf(__('Invalid meta type "%s" for field "%s" in custom post type "%s". The only allowed types are "%s"', 'wpify'), $args['type'], $args['meta_key'], $this->get_entity_name(), var_export($allowed_types, true)));
      }
    }
  }

  public function get_custom_fields(): array
  {
    return $this->custom_fields;
  }

  /**
   * Get a single field value
   *
   * @param $post_id
   * @param string $key
   * @param bool $single
   * @return mixed
   */
  public function get_field($post_id, $key = '', bool $single = true)
  {
    if ($this->get_type() === 'cpt') {
      $def = $this->get_custom_field_definition($key);

      $value = get_post_meta($post_id, $key, $def['single']);

      if ($value === null) {
        return $def['default'];
      }

      return $value;
    }

    return null;
  }

  /**
   * Save custom field value
   *
   * @param $post_id
   * @param $meta_key
   * @param $meta_value
   * @return bool
   */
  public function save_field($post_id, $meta_key, $meta_value)
  {
    if ($this->get_type() === 'cpt') {
      return boolval(update_post_meta($post_id, $meta_key, $meta_value));
    }

    return false;
  }

  private function get_custom_field_definition($key)
  {
    if (empty($this->get_custom_fields()) || $this->get_type() !== 'cpt') {
      return null;
    }

    foreach($this->get_custom_fields() as $field) {
      if ($key === $field['meta_key']) {
        return $field;
      }
    }

    return null;
  }

  public function register_fields()
  {
    if (empty($this->get_custom_fields()) || $this->get_type() !== 'cpt') {
      return;
    }

    foreach($this->get_custom_fields() as $field) {
      register_meta($field['object_type'], $field['meta_key'], $field);
    }
  }
}
