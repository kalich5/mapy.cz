<?php

namespace WPifyMapyCz;

use WPifyMapyCz\Controllers\MapController;
use Wpify\Core\AbstractComponent;

/**
 * Class Frontend
 * @package WPifyMapyCz
 * @property Plugin $plugin
 */
class Frontend extends AbstractComponent
{
  const SHORTCODE_TAG = 'wpify-mapy-cz';

  public function setup()
  {
    add_shortcode(self::SHORTCODE_TAG, [$this, 'render_map']);
  }

  public function render_map($atts)
  {
    $atts = shortcode_atts(
      ['id' => null],
      $atts,
      'wpify-mapy-cz'
    );

    if (!$atts['id']) {
      return __('Please add id param to the shortcode', 'wpify-mapy-cz');
    }

    if (!is_numeric($atts['id'])) {
      return __('Please provide valid ID', 'wpify-mapy-cz');
    }
    return $this->get_map_markup($atts['id']);
  }

  public function get_map_markup($map_id)
  {
    $data = $this->plugin->get_controller(MapController::class)->get_map($map_id);

    if (!$data) {
      return __('Please provide valid ID', 'wpify-mapy-cz');
    }

    ob_start(); ?>
    <style type="text/css">.smap img {max-width: none !important;}</style>
    <div class="mapy-cz__map" data-mapycz="<?php echo esc_attr(json_encode($data)); ?>"
         style="height: <?php echo esc_attr($data['height']) ?>; width: <?php echo esc_attr($data['width']); ?>"></div>
    <?php
    return ob_get_clean();
  }
}
