<?php

namespace WPifyMapyCz\Managers;

use Wpify\Core\AbstractManager;
use WPifyMapyCz\Plugin;
use WPifyMapyCz\Blocks\MapyCzBlock;
use WPifyMapyCz\Blocks\MapEditorBlock;
/** @property Plugin $plugin */
class BlocksManager extends AbstractManager
{
    protected $modules = [MapyCzBlock::class, MapEditorBlock::class];
}
