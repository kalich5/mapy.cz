<?php

namespace WPifyMapyCz\Managers;

use Wpify\Core\AbstractManager;
use WPifyMapyCz\Plugin;
use WPifyMapyCz\Cpt\MarkerPostType;
use WPifyMapyCz\Cpt\MapPostType;
/**
 * Class CptManager
 * @package Wpify\Managers
 * @property Plugin $plugin
 */
class CptManager extends AbstractManager
{
    protected $modules = [MarkerPostType::class, MapPostType::class];
}
