<?php

namespace WPifyMapyCz\Managers;

use Wpify\Core\AbstractManager;
use WPifyMapyCz\Plugin;
use WPifyMapyCz\Repositories\MarkerRepository;
use WPifyMapyCz\Repositories\MapRepository;
/**
 * Class RepositoriesManager
 * @package Wpify\Managers
 * @property Plugin $plugin
 */
class RepositoriesManager extends AbstractManager
{
    protected $modules = [MarkerRepository::class, MapRepository::class];
}
