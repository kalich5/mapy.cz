<?php

namespace WPifyMapyCz\Models;

use WPifyMapyCz\Plugin;
use WPifyMapyCz\Repositories\MarkerRepository;
use Wpify\Core\AbstractPostTypeModel;

/**
 * Class MapModel
 * @package WPifyMapyCz\Models
 * @property Plugin $plugin
 */
class MapModel extends AbstractPostTypeModel
{
  private $center_lat;
  private $center_lng;
  private $zoom;
  private $width;
  private $height;
  private $markers = [];
  private $markers_categories = [];
  private $categories_ids = [];
  private $categories = [];
  private $hash;
  private $show_info_window;
  private $center_auto;
  private $default_layer;

  public function setup()
  {
    if ($this->get_id()) {
      $this->center_lat       = $this->get_custom_field('latitude');
      $this->center_lng       = $this->get_custom_field('longitude');
      $this->zoom             = $this->get_custom_field('zoom');
      $this->center_auto      = $this->get_custom_field('auto_center_zoom');
      $this->default_layer    = $this->get_custom_field('layer_type');
      $this->width            = $this->get_custom_field('width');
      $this->height           = $this->get_custom_field('height');
      $this->show_info_window = $this->get_custom_field('show_info_window');

      try {
        foreach (json_decode($this->get_custom_field('markers')) as $marker_id) {
          $this->markers[] = $this->plugin->get_repository(MarkerRepository::class)->get($marker_id);
        }
      } catch (Exception $e) {
        $this->markers = [];
      }

      $this->categories_ids     = apply_filters('mapy_cz_map_categories_ids', $this->categories_ids, $this);
      $this->categories         = apply_filters('mapy_cz_map_categories', $this->categories, $this);
      $this->markers_categories = apply_filters('mapy_cz_map_markers_categories', $this->markers_categories, $this);
      $this->hash               = rand();
    }
  }

  /**
   * @return mixed
   */
  public function get_center_lat()
  {
    return $this->center_lat;
  }

  /**
   * @return mixed
   */
  public function get_center_lng()
  {
    return $this->center_lng;
  }

  /**
   * @return mixed
   */
  public function get_zoom()
  {
    return $this->zoom;
  }

  /**
   * @return mixed
   */
  public function get_width()
  {
    return $this->width;
  }

  /**
   * @return mixed
   */
  public function get_height()
  {
    return $this->height;
  }

  /**
   * @return mixed
   */
  public function get_markers()
  {
    return $this->markers;
  }

  /**
   * @return mixed
   */
  public function get_markers_categories()
  {
    return $this->markers_categories;
  }

  /**
   * @return mixed
   */
  public function get_categories_ids()
  {
    return $this->categories_ids;
  }

  /**
   * @return mixed
   */
  public function get_categories()
  {
    return $this->categories;
  }

  public function get_hash()
  {
    return $this->hash;
  }

  /**
   * @return mixed
   */
  public function get_show_info_window()
  {
    return $this->show_info_window;
  }

  /**
   * @return mixed
   */
  public function get_center_auto()
  {
    return $this->center_auto;
  }

  /**
   * @return mixed
   */
  public function get_default_layer()
  {
    return $this->default_layer;
  }
}
