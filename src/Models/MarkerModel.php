<?php

namespace WPifyMapyCz\Models;

use Wpify\Core\AbstractPostTypeModel;

class MarkerModel extends AbstractPostTypeModel
{
  private $latitude;
  private $longitude;
  private $categories_ids = [];
  private $categories = [];
  private $description = '';
  private $address = '';
  private $custom_marker;

  public function setup()
  {
    if ($this->get_id()) {
      $this->latitude       = $this->get_custom_field('latitude');
      $this->longitude      = $this->get_custom_field('longitude');
      $this->custom_marker  = $this->get_custom_field('custom_marker');
      $this->description    = $this->get_custom_field('description');
      $this->address        = $this->get_custom_field('address');
      $this->categories_ids = apply_filters('mapy_cz_marker_categories_ids', $this->categories_ids, $this);
      $this->categories     = apply_filters('mapy_cz_marker_categories_ids', $this->categories, $this);
    }
  }

  /**
   * @return mixed
   */
  public function get_categories_id()
  {
    return $this->categories_id;
  }

  /**
   * @return mixed
   */
  public function get_categories()
  {
    return $this->categories;
  }

  public function get_custom_marker()
  {
    return $this->custom_marker;
  }

  public function to_array()
  {
    return [
      'id'               => $this->get_id(),
      'title'            => $this->get_title(),
      'description'      => $this->get_description(),
      'address'          => $this->get_address(),
      'latitude'         => $this->get_latitude(),
      'longitude'        => $this->get_longitude(),
    ];
  }

  public function get_description()
  {
    return $this->description;
  }

  /**
   * @return mixed
   */
  public function get_latitude()
  {
    return $this->latitude;
  }

  /**
   * @return mixed
   */
  public function get_longitude()
  {
    return $this->longitude;
  }

  public function get_address()
  {
    return $this->address;
  }
}
