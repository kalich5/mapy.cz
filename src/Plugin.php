<?php

namespace WPifyMapyCz;

use ComposePress\Core\Exception\ContainerInvalid;
use ComposePress\Core\Exception\ContainerNotExists;
use Exception;
use WPifyMapyCz\Assets;
use Wpify\Core\Interfaces\RepositoryInterface;
use Wpify\Core\AbstractPlugin as PluginBase;
use Wpify\Core\View;
use WPifyMapyCz\Managers\BlocksManager;
use WPifyMapyCz\Managers\CptManager;
use WPifyMapyCz\Managers\RepositoriesManager;

/**
 * Class Plugin
 * @package Wpify
 */
class Plugin extends PluginBase
{
  /** Plugin version */
  public const VERSION = '1.0.0';

  /** Plugin slug name */
  public const PLUGIN_SLUG = 'wpify-mapy-cz';

  /** Plugin namespace */
  public const PLUGIN_NAMESPACE = '\\' . __NAMESPACE__;

  /** @var Frontend */
  private $frontend;

  /** @var CptManager */
  private $cpt_manager;

  /** @var RepositoriesManager */
  private $repositories_manager;

  /** @var BlocksManager */
  private $blocks_manager;

  /** @var Assets */
  private $assets;

  /** @var View */
  private $view;
  /**
   * @var Admin
   */
  private $admin;

  /**
   * Plugin constructor.
   *
   * @param Frontend $frontend
   * @param RepositoriesManager $repositories_manager
   * @param CptManager $cpt_manager
   * @param BlocksManager $blocks_manager
   * @param Assets $assets
   * @param View $view
   * @param Admin $admin
   *
   * @throws ContainerInvalid
   * @throws ContainerNotExists
   */
  public function __construct(
    Frontend $frontend,
    RepositoriesManager $repositories_manager,
    CptManager $cpt_manager,
    BlocksManager $blocks_manager,
    Assets $assets,
    View $view,
    Admin $admin
  ) {
    $this->frontend             = $frontend;
    $this->cpt_manager          = $cpt_manager;
    $this->repositories_manager = $repositories_manager;
    $this->blocks_manager       = $blocks_manager;
    $this->assets               = $assets;
    $this->view                 = $view;
    $this->admin                = $admin;

    parent::__construct();
  }

  public function get_frontend(): Frontend
  {
    return $this->frontend;
  }

  public function get_repositories_manager(): RepositoriesManager
  {
    return $this->repositories_manager;
  }

  /**
   * @param string $class
   *
   * @return RepositoryInterface
   */
  public function get_repository(string $class)
  {
    return $this->repositories_manager->get_module($class);
  }

  public function get_cpt_manager(): CptManager
  {
    return $this->cpt_manager;
  }

  public function get_cpt(string $class)
  {
    return $this->cpt_manager->get_module($class);
  }


  public function get_controller(string $class)
  {
    return $this->plugin->create_component($class);
  }

  public function get_blocks_manager()
  {
    return $this->blocks_manager;
  }

  public function get_assets(): Assets
  {
    return $this->assets;
  }

  /**
   * Print styles in theme
   *
   * @param $handles
   */
  public function print_assets(string ...$handles)
  {
    $this->assets->print_assets($handles);
  }

  public function get_view(): View
  {
    return $this->view;
  }

  /**
   * @return Admin
   */
  public function get_admin(): Admin
  {
    return $this->admin;
  }


  /**
   * Method to check if plugin has its dependencies. If not, it silently aborts
   * @return bool
   */
  protected function get_dependencies_exist()
  {
    return true;
  }

  /**
   * @return bool
   * @throws Exception
   */
  protected function load_components()
  {
    // Conditionally lazy load components with $this->load()
  }

  /**
   * Plugin activation and upgrade
   *
   * @param $network_wide
   *
   * @return void
   */
  public function activate($network_wide)
  {
  }

  /**
   * Plugin de-activation
   *
   * @param $network_wide
   *
   * @return void
   */
  public function deactivate($network_wide)
  {
  }

  /**
   * Plugin uninstall
   * @return void
   */
  public function uninstall()
  {
  }
}
