<?php

namespace WPifyMapyCz\Repositories;

use Wpify\Core\AbstractPostTypeRepository;
use WPifyMapyCz\Cpt\MapPostType;

/**
 * @property Plugin $plugin
 */
class MapRepository extends AbstractPostTypeRepository
{
  public function post_type(): MapPostType
  {
    return $this->plugin->get_cpt(MapPostType::class);
  }
}
