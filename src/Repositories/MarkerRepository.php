<?php

namespace WPifyMapyCz\Repositories;

use Wpify\Core\AbstractPostTypeRepository;
use WPifyMapyCz\Cpt\MarkerPostType;

/**
 * @property Plugin $plugin
 */
class MarkerRepository extends AbstractPostTypeRepository
{
  public function post_type(): MarkerPostType
  {
    return $this->plugin->get_cpt(MarkerPostType::class);
  }
}
