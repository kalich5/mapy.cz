<?php // phpcs:disable PSR1.Files.SideEffects.FoundWithSymbols

/*
 * Plugin Name:       WPify Mapy.cz
 * Description:       Easily embed free mapy.cz maps on your posts / pages / Custom Post Types
 * Version:           1.0
 * Requires PHP:      7.3.0
 * Requires at least: 5.5
 * Author:            WPify
 * Author URI:        https://www.wpify.io/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       wpify-mapy-cz
 * Domain Path: /languages
*/

use Dice\Dice;
use WPifyMapyCz\Plugin;

if (!defined('WPIFY_MAPY_CZ_MIN_PHP_VERSION')) {
  define('WPIFY_MAPY_CZ_MIN_PHP_VERSION', '7.3.0');
}

/**
 * Singleton instance function. We will not use a global at all as that defeats the purpose of a singleton
 * and is a bad design overall
 * @SuppressWarnings(PHPMD.StaticAccess)
 * @return WPifyMapyCz\Plugin
 */
function wpify_mapy_cz(): Plugin
{
  return wpify_mapy_cz_container()->create(Plugin::class);
}

/**
 * This container singleton enables you to setup unit testing by passing an environment file to map classes in Dice
 *
 * @param string $env
 *
 * @return Dice
 */
function wpify_mapy_cz_container($env = 'production'): Dice
{
  static $container;

  if (empty($container)) {
    $container = new Dice();
    include __DIR__ . "/config-{$env}.php";
  }

  return $container;
}

/**
 * Init function shortcut
 */
function wpify_mapy_cz_init()
{
  wpify_mapy_cz()->init();
}

function wpify_mapy_cz_load_textdomain()
{
  load_plugin_textdomain('wpify-mapy-cz', false, dirname(plugin_basename(__FILE__)) . '/languages');
}

/**
 * Activate function shortcut
 */
function wpify_mapy_cz_activate($network_wide)
{
  register_uninstall_hook(__FILE__, 'wpify_mapy_cz_uninstall');
  wpify_mapy_cz()->init();
  wpify_mapy_cz()->activate($network_wide);
}

/**
 * Deactivate function shortcut
 */
function wpify_mapy_cz_deactivate($network_wide)
{
  wpify_mapy_cz()->deactivate($network_wide);
}

/**
 * Uninstall function shortcut
 */
function wpify_mapy_cz_uninstall()
{
  wpify_mapy_cz()->uninstall();
}

/**
 * Error for older php
 */
function wpify_mapy_cz_php_upgrade_notice()
{
  $info = get_plugin_data(__FILE__);
  _e(
    sprintf(
      '
      <div class="error notice">
        <p>
          Opps! %s requires a minimum PHP version of ' . WPIFY_MAPY_CZ_MIN_PHP_VERSION . '. Your current version is: %s.
          Please contact your host to upgrade.
        </p>
      </div>
      ',
      $info['Name'],
      PHP_VERSION
    )
  );
}

/**
 * Error if vendors autoload is missing
 */
function wpify_mapy_cz_php_vendor_missing()
{
  $info = get_plugin_data(__FILE__);
  _e(
    sprintf(
      '
      <div class="error notice">
        <p>Opps! %s is corrupted it seems, please re-install the plugin.</p>
      </div>
      ',
      $info['Name']
    )
  );
}

/*
 * We want to use a fairly modern php version, feel free to increase the minimum requirement
 */
if (version_compare(PHP_VERSION, WPIFY_MAPY_CZ_MIN_PHP_VERSION) < 0) {
  add_action('admin_notices', 'wpify_mapy_cz_php_upgrade_notice');
} else {
  if (file_exists(__DIR__ . '/vendor/autoload.php')) {
    include_once __DIR__ . '/vendor/autoload.php';
    wpify_mapy_cz_load_textdomain();
    add_action('plugins_loaded', 'wpify_mapy_cz_init');
    register_activation_hook(__FILE__, 'wpify_mapy_cz_activate');
    register_deactivation_hook(__FILE__, 'wpify_mapy_cz_deactivate');
  } else {
    add_action('admin_notices', 'wpify_mapy_cz_php_vendor_missing');
  }
}
