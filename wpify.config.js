module.exports = {
  config: {
    build: 'build',
    entry: {
      'plugin': './assets/plugin.js',
      'block-mapy-cz-backend': './assets/blocks/mapy-cz/backend.jsx',
      'map-editor-backend': './assets/blocks/map-editor/backend.jsx',
    },
  },
  webpack: (config) => {
    config.output.filename = '[name].js';
    config.output.chunkFilename = '[name].js';
    config.optimization.splitChunks = false;
    delete config.output.publicPath;

    return config;
  },
  browserSync: (config) => {
    return config;
  },
};
